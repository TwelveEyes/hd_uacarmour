**Corporate Armour**

GZDoom actor name: `CorporateArmour`
Loadout code: `awc` (worn), `arc` (spare)

An advanced set of combat armour built and developed by the UAC. The outer plates are rigid carbon nanocomposite that reliably block penetration from high velocity projectiles. The bodyglove is a fully enclosed kevlar weave with a dilatant fluid backing to disperse blunt force. The dilatant is a decent heat reservoir, protecting the wearer from sudden and extreme heat spikes. Punctures into the dilatant layer activate the nanites in the fluid, which attempt to repair damage. The nanites get power from a small internal power system in the suit underlayer charged from the wearer's body heat; the armour will not regenerate if not worn.

Ironically, combat after action reports showed that personnel issued this armour were statistically more likely to be casualties. Interviews with survivors revealed "feelings of invincibility" and "delusions of grandeur" when wearing the armour.

- Better protection that battle armour.
- Lighter than battle armour.
- Protects from environmental damage like a radsuit.
- Can block 7.76, sometimes. Don't rely on it.
- Self-healing.
- Does not make you invincible.
