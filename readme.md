### Notes

---

- Corporate armour will spawn rarely in place of blue armour, and very rarely in place of green armour.
- Loadout code is `awu` for worn armour, `aru` for spare.
- This armour is more protective and a little lighter than battle armour, with good environmental resistance.
- The armour self-heals damage to it over time. It only self-heals when worn.
